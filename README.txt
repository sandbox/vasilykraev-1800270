Module: LittleSMS Gateway
Author: Vasily Kraev

DESCRIPTION
-------
This module enables the SMS Framework to sent SMS via LittleSMS (littlesms.ru)

REQUIREMENTS
-------
- SMS Framework module
- An LittleSMS account

INSTALL
-------
1) Install the SMS Framework module from http://drupal.org/project/smsframework
2) Drop this modul into a subdir of your choice (regarding the Drupal standards)
    for example sites/all/modules
3) enable the module under admin/build/modules
4) Configure the module under admin/smsframework/gateways/littlesms
  a) Enter your LittleSMS login
  b) Enter your API-Key (can be found here: http://littlesms.ru/my/settings/api)
  c) Enter a sender identifier wich should be used (can be found here:
     http://littlesms.ru/my/settings/sender )
  d) Your settings will be validated when you click save.
     If you recive an error, it will hopefully give you futher instructions on
     what to do. If you got an error without instructions, compare the error
     code with the list in the Gateway documentation.
5) Use the SMS Framework module or any other module that uses it.